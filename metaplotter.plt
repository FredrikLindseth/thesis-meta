# title
set title "Meta-thesis" font ",20"

# CSV-format on the data
set datafile separator ","

# the x-axis is time
set xdata time

# explain the format in the data-file
set timefmt "%Y-%m-%d"

# range of the y-axes
set yrange [ 0 : 25000 ]
set y2range [ 0 : 110 ]

# range of the x-axis
set xrange [ "2017-08-01": "2018-06-01" ]

# use the first line in the data to name each plot
set key autotitle columnhead left top

# label the axis
set ylabel "Number of words"
set y2label "Number of other things"

# format the x-axis time format:
# %b is month "January", %d is day in month "01", %Y is year "2018"
set format x "%b/%Y"

# set the ticks on the x-axis, to many and they overlap
set xtics ("2017-09-01","2017-11-01", "2018-01-01", "2018-03-01","2018-05-01" ) 

# disable mirroring of ticks so the y-axis looks nice
set ytics nomirror 
set y2tics 10 

set grid

# put the legend on the top and type 3
set key box 3 top

#pdf output
# set terminal pdf
# set output 'metaplot.pdf'

# SVG output
set terminal svg
set output 'metaplot.svg'

# custom labels to explain something
set label 1 at  "2017-11-18", 3750 "[1]"
set label 2 at  "2018-01-27", 7500 "[2]"

# do the plotting, each comma is a new plot.
plot "metaplot.dat" using 1:2 w lines ls 1 lw 3 lc 0, '' using 1:3 w lines ls 1 lw 1.5 lc 1 axes x1y2,'' using 1:4 w lines ls 1 lw 1.5 lc 2 axes x1y2, '' using 1:5 w lines ls 1 lw 1.5 lc 3 axes x1y2, 

# ls = linestyle,  default 1
# lw = linewidth,  default 2
# lc = linecolour, default 0