#! /bin/bash
# Count words, pages, sources and figures in a latex-project
# Author: Fredrik Lindseth
# Date created :  2017-08-24
# Date modified : 2018-03-05
# URL: https://github.com/FredrikLindseth/thesis-meta

fileWithoutExtension=${1%%.*}
file=${1}

# count words in the tex-document, use tex as parameter when running
if [ ${file: -4} == ".tex" ]
    then
    wc=`detex $fileWithoutExtension| wc -w`

# count words in the finished PDF, use the pdf as parameter when running
elif [ ${file: -4} == ".pdf" ]
  then    
  wc=`pdftotext $file - | wc -w`
  
else 
    echo "Only valid input is *.pdf or *.tex"
fi

# count pages of the compiled PDF
pages=$(pdfinfo *.pdf | grep Pages| grep -oP '\d+\d')

# count occurences of "author" in the bibliograpyh
sources=$(grep author *.bib | wc -l)

# combine the whole project into a single tex-file
echo $(perl ../latexpand main.tex > main-expanded.tex)
# count figures and tables
figures=$(($(grep begin{figure} main-expanded.tex | wc -l) +  $(grep begin{table} main-expanded.tex | wc -l)))
# remove the combined file after use
echo $(rm main-expanded.tex)

# save everything to the data-file
echo "$(date +%F),$wc,$pages,$sources,$figures" >> metaplot.dat
