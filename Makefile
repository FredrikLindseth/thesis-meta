# Count words, update plot, add build-tex-project here to build the PDF also
#all: build-latex-project updateWeb wordcount-pdf convert-pdf
all: wordcount-pdf convert_svg_to_png

# count words in your tex-file
wordcount-tex :
	./wordcount.sh main.tex
	gnuplot metaplotter.plt

# count words in your compiled PDF
wordcount-pdf:
	./wordcount.sh example.pdf
	gnuplot metaplotter.plt

# convert the SVG to a PNG
convert_svg_to_png:
	convert -density 150 metaplot.svg -quality 90 metaplot.png




 #						#
### Bonus Make-targets ###
 #						#

# build your PDF
build-tex-project:
	@#Build a PDF with lualatex
	lualatex main
	bibtex main
	makeglossaries main 
	lualatex main
	@# lualatex main
	mv main.pdf Fredrik_Lindseth_thesis.pdf

# Send your file to the server for your supervisor to view
updateWeb:
	scp /home/user/Documents/UiB/masteren/tex/Fredrik_Lindseth_thesis.pdf user@hostname.no:/var/www/fredriklindseth/assets/
