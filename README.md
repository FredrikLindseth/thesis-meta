# Plot the development of your thesis!  

This plots words, figures, sources and pages of your thesis and creates a pretty plot to look at for instant gratification and motivation to keep writing!

![metaplot.png](sample-data/metaplot.png)

# Do this

1. `git clone git@github.com:FredrikLindseth/thesis-meta.git`
2. place the [counter.sh](counter.sh) and [Makefile](Makefile) in the same folder as your main tex-file and PDF
    - Remove the sample data. `rm -rf sample-data`
3. open a terminal and type `make` 
4. view your pretty plot [metaplot.svg](sample-data/metaplot.svg)
5. ???
6. profit!

# How it works
It is assumed that this git repo is placed in the root-folder of your project and your tex-project along with bibliograpy and PDF file are placed here.


The number of words, pages, figures and sources are counted automatically with the [counter.sh](counter.sh)-script. This script takes a PDF or a tex-file as parameter, depening on where you want to count your words.

It counts words both in tex-projects or compiled PDFs, and counts the pages of the PDF. The script asssumes that there is only one PDF in the folder. 

This script also counts the number of `author`s in your bibliography-file (as long as it ends with .bib). It assumes there is only one .bib-file in the folder. 

It also combines all the tex-files `\include`d or `\input`ted in your `main.tex` and counts all `begin{figure}` and `begin{table}` and combines this to a single `figures`-value.

## Count words in tex vs pdf
Counting words in your tex project is more accurate, but its impossible to compare with other theses. So for ease of comparison it defaults to count-pdf. 

Change line 3 in [Makefile](Makefile) to wordcount-tex to count in your tex instead.

# Makefile
The [Makefile](Makefile) counts your words, updates your plot and converts the SVG (which is fine for version control) to a png (which is nice for to put in the README).

There are also some targets for building your tex project and a target to send your PDF to a fileserver to send to i.e. your  supervisor.

## Requirements 
- linux (for Make, grep, pdfinfo, detex, pdftotext) [0]
- gnuplot (for plotting)
- perl (for expanding and combining all the tex-files with [latexexpand](latexexpand))
- texlive (if you want to build your project with the [Makefile](Makefile) )

### I.e for Ubuntu
`sudo apt-get install gnuplot perl texlive`

# Something for comparison
I counted ~12 master theses in microelectronics to see what was average values were and the table below shows the results. 


| Metric     | Average           | 1   σ    |
|------------|-------------------|----------|
| Words      | 29219             | 9519     |
| Pages [1]  | 119               | 16       |
| Sources    | 46                | 17       |
| Figures [2]| ~1 per page       | -        |

[0] It is very possible to run these commands on a windows machine, especially now with the Ubuntu subsystem for windows 10

[1] The pages includes blank pages, table of contents, everything

[2] Figures are pictures and tables
